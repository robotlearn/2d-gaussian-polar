"""
d and theta are two independant random variables distributed as such:
- d ~ N(mu_d, sigma_d)
- theta ~ N(mu_theta, sigma_theta)

We would like to plot the 2d distributions of (d, theta) in polar coordinates.
"""
import numpy as np
import matplotlib.pyplot as plt


mean_d: float = 1
sigma_d: float = 0.05

mean_theta: float = -np.pi/4
sigma_theta: float = 1

plot_resolution: int = 100

def gaussian_2d(
    d: float,
    theta: float
) -> float:
    coeff: float = 1 / (2 * np.pi * sigma_d * sigma_theta)
    return coeff * np.exp(
        - 0.5 * (
            (
                ((d - mean_d)**2)
                /
                sigma_d
            )
            +
            (
                ((theta - mean_theta)**2)
                /
                sigma_theta
            )
        )
    )

# Plot the 'marginal' distributions (not really)
list_d: np.ndarray = np.linspace(
    mean_d - 3 * sigma_d,
    mean_d + 3 * sigma_d,
    plot_resolution
)
list_theta: np.ndarray = np.linspace(-np.pi, np.pi, plot_resolution)
marginal_d: np.ndarray = np.array(
    [
        gaussian_2d(
            d=d,
            theta=mean_theta
        )
        for d in list_d
    ]
)
# plt.plot(list_d, marginal_d)

marginal_theta: np.ndarray = np.array(
    [
        gaussian_2d(
            d=mean_d,
            theta=theta
        )
        for theta in list_theta
    ]
)
# plt.plot(list_theta, marginal_theta)

#########################################################
# Plot the contour lines of the 2d density in polar coordinates

def gaussian_2d_polar(x: float, y: float) -> float:
    return gaussian_2d(
        d=np.sqrt(x**2 + y**2),
        theta=np.arctan2(y, x)
    )

grid_max: float = 2 * mean_d
list_x: np.ndarray = np.linspace(-grid_max, grid_max, plot_resolution)
list_y: np.ndarray = np.linspace(-grid_max, grid_max, plot_resolution)
grid: tuple[np.ndarray, np.ndarray] = np.meshgrid(
    list_x,
    list_y
)

xx, yy = grid
zz = gaussian_2d_polar(xx, yy)

# Plot the mean distance
x_unit_circle: np.ndarray = np.array(
    [
        mean_d * np.cos(t) for t in np.linspace(-np.pi, np.pi, plot_resolution)]
)
y_unit_circle: np.ndarray = np.array(
    [
        mean_d * np.sin(t)
        for t in np.linspace(-np.pi, np.pi, plot_resolution)]
)
plt.plot(
    x_unit_circle, y_unit_circle,
    color='white'
)

# Plot the mean direction
x_mean_theta: np.ndarray = np.array(
    [
        r * np.cos(mean_theta)
        for r in np.linspace(0, mean_d)
    ]
)
y_mean_theta: np.ndarray = np.array(
    [
        r * np.sin(mean_theta)
        for r in np.linspace(0, mean_d)
    ]
)
plt.plot(
    x_mean_theta, y_mean_theta,
    color='white'
)

plt.contourf(list_x, list_y, zz)
plt.axis('scaled')
plt.colorbar()
plt.axhline(0, color='white')
plt.axvline(0, color='white')
plt.show()
